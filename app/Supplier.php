<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    public function groupsuppliers()
    {
        return $this->belongsToMany('App\Groupsupplier')->withTimestamps();
    }
    public function purchases()
    {
        return $this->belongsToMany('App\Purchase')->withTimestamps();
    }
}
