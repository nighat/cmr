<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function items()
    {
        return $this->belongsToMany('App\Item')->withTimestamps();
    }
    public function purchases()
    {
        return $this->belongsToMany('App\Purchase')->withTimestamps();
    }
    public function retires()
    {
        return $this->belongsToMany('App\Retire')->withTimestamps();
    }
}
