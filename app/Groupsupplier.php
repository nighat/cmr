<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Groupsupplier extends Model
{
    public function suppliers()
    {
        return $this->belongsToMany('App\Supplier')->withTimestamps();
    }
}
