<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{

    public function products()
    {
        return $this->belongsToMany('App\Product')->withTimestamps();
    }
    public function suppliers()
    {
        return $this->belongsToMany('App\Supplier')->withTimestamps();
    }
}
