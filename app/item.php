<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class item extends Model
{
    public function products()
    {
        return $this->belongsToMany('App\Product')->withTimestamps();
    }
}
