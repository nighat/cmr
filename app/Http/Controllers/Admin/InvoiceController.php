<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Invoice;
use App\Client;
use Brian2694\Toastr\Facades\Toastr;
class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $invoices= Invoice::latest()->get();
        return view('admin.invoice.index',compact('invoices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clients = Client::all();
        return view('admin.invoice.create',compact('clients'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $client= Client::find($request->client_id);
        $names = array(
        );
        $prices = array(
        );
        $qtys = array(
        );
        $invoice = new Invoice;
        $invoice->date = $request->date;
        $invoice->unique_id = $request->unique_id;
        $invoice->attendees = serialize($names);
        $invoice->attendees = serialize($prices);
        $invoice->attendees = serialize($qtys);
//        $invoice->name = $request->name;
//        $invoice->price = $request->price;
//        $invoice->qty = $request->qty;
        $invoice->save();
      // $invoice->clients()->attach($request->clients);
        Toastr::success('invoice Successfully Saved :)','Success');
        return redirect(route('admin.invoice.index',compact('client')));
//        $invoice = new Invoice;
//        $invoice->date = $request->date;
//        $invoice->unique_id = $request->unique_id;
//        $invoice->name = $request->name;
//        $invoice->price = $request->price;
//        $invoice->qty = $request->qty;
//        $invoice->save();
//      // $invoice->clients()->attach($request->clients);
//        Toastr::success('invoice Successfully Saved :)','Success');
//        return redirect(route('admin.invoice.index',compact('client')));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $clients = Client::all();
        $invoice= Invoice::where('id',$id)->first();
        return view('admin.invoice.edit',compact('invoice','clients'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $invoice = Invoice::find($id);

        $invoice->date = $request->date;
        $invoice->name = $request->name;
        $invoice->price = $request->price;
        $invoice->qty = $request->qty;
        $invoice->save();
        //$client->groupclients()->sync($request->groupclients);
        Toastr::success('invoice Successfully Updated :)','Success');
        return redirect(route('admin.invoice.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Invoice::where('id',$id)->delete();
        return redirect()->back();
    }
}
