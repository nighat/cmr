<?php

namespace App\Http\Controllers\Admin;

use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Purchase;
use App\Supplier;
use Brian2694\Toastr\Facades\Toastr;
use Carbon\Carbon;
class PurchaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $purchases= Purchase::latest()->get();
        return view('admin.purchase.index',compact('purchases'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $products = Product::all();
        $suppliers = Supplier::all();
        return view('admin.purchase.create',compact('products','suppliers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $purchase = new Purchase;
        $purchase->date = $request->date;
        $purchase->purchase_quantity = $request->purchase_quantity;
        $purchase->buy_price = $request->buy_price;
        $purchase->sell_price = $request->sell_price;
        $purchase->note = $request->note;
        $purchase->save();

        $purchase->products()->attach($request->products);
        $purchase->suppliers()->attach($request->suppliers);

        Toastr::success('Purchase Successfully Saved :)','Success');
        return redirect(route('admin.purchase.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show( Purchase $purchase)
    {
        return view('admin.purchase.show',compact('purchase'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $suppliers = Supplier::all();
        $products = Product::all();
        $purchase = Purchase::where('id',$id)->first();
        return view('admin.purchase.edit',compact('purchase','suppliers','products'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


        $purchase = Purchase::find($id);
        $purchase->date = $request->date;
        $purchase->purchase_quantity = $request->purchase_quantity;
        $purchase->buy_price = $request->buy_price;
        $purchase->sell_price = $request->sell_price;
        $purchase->note = $request->note;
        $purchase->save();
        $purchase->products()->sync($request->products);
        $purchase->suppliers()->sync($request->suppliers);
        Toastr::success('Purchase Successfully Updated :)','Success');
        return redirect(route('admin.purchase.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Purchase::where('id',$id)->delete();
        return redirect()->back();
    }
}
