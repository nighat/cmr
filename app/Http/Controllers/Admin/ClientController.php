<?php

namespace App\Http\Controllers\Admin;

use App\Groupclient;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Client;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = Client::latest()->get();
        return view('admin.client.index', compact('clients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $groupclients = Groupclient::all();
        return view('admin.client.create', compact('groupclients'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Client $client)
    {
        $client = new Client;
        $client->name = $request->name;
        $client->p_name = $request->p_name;
        $client->phone = $request->phone;
        $client->email = $request->email;
        $client->address= $request->address;
        $client->city= $request->city;

        $client->save();

        $client->groupclients()->attach($request->groupclients);
        Toastr::success('Client Successfully Saved :)','Success');
        return redirect(route('admin.client.index'));

}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {
        return view('admin.client.show',compact('client'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit( $id)
    {
        $groupclients = Groupclient::all();
        $client= Client::where('id',$id)->first();
        return view('admin.client.edit',compact('client','groupclients'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $client = Client::find($id);
        $client->name = $request->name;
        $client->p_name = $request->p_name;
        $client->phone = $request->phone;
        $client->email = $request->email;
        $client->address= $request->address;
        $client->city= $request->city;
        $client->save();
        $client->groupclients()->sync($request->groupclients);
        Toastr::success('Client Successfully Updated :)','Success');
        return redirect(route('admin.client.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Client::where('id',$id)->delete();
        return redirect()->back();
    }




}
