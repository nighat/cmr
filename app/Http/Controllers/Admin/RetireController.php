<?php

namespace App\Http\Controllers\Admin;

use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Retire;
use App\Supplier;
use Brian2694\Toastr\Facades\Toastr;
use Carbon\Carbon;

class RetireController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $retires= Retire::latest()->get();
        return view('admin.retire.index',compact('retires'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $products = Product::all();
        $suppliers = Supplier::all();
        return view('admin.retire.create',compact('products','suppliers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $retire = new Retire;
        $retire->date = $request->date;
        $retire->return_quantity = $request->return_quantity;
        $retire->buy_price = $request->buy_price;
        $retire->note = $request->note;
        $retire->save();

        $retire->products()->attach($request->products);
        $retire->suppliers()->attach($request->suppliers);

        Toastr::success('Return Successfully Saved :)','Success');
        return redirect(route('admin.retire.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Retire $retire)
    {
        return view('admin.retire.show',compact('retire'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $suppliers = Supplier::all();
        $products = Product::all();
        $retire = Retire::where('id',$id)->first();
        return view('admin.retire.edit',compact('retire','suppliers','products'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


        $retire = Retire::find($id);
        $retire->date = $request->date;
        $retire->return_quantity = $request->return_quantity;
        $retire->buy_price = $request->buy_price;
        $retire->note = $request->note;
        $retire->save();
        $retire->products()->sync($request->products);
        $retire->suppliers()->sync($request->suppliers);
        Toastr::success('Return Successfully Updated :)','Success');
        return redirect(route('admin.retire.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Retire::where('id',$id)->delete();
        return redirect()->back();
    }
}