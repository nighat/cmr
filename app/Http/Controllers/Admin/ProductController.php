<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Item;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Carbon\Carbon;
class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products= Product::latest()->get();
        return view('admin.product.index',compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $items = Item::all();
        return view('admin.product.create',compact('items'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $image = $request->file('image');
        if(isset($image))
        {
//            make unipue name for image
            $currentDate = Carbon::now()->toDateString();
            $imageName  = '-'.$currentDate.'-'.uniqid().'.'.$image->getClientOriginalExtension();
            if(!Storage::disk('public')->exists('product'))
            {
                Storage::disk('public')->makeDirectory('product');
            }
            $productImage = Image::make($image)->resize(1600,1066)->stream();
            Storage::disk('public')->put('product/'.$imageName,$productImage);
        } else {
            $imageName = "default.png";
        }

        $product = new Product;
        $product->name = $request->name;
        $product->image = $imageName;
        $product->description = $request->description;
        $product->product_unit= $request->product_unit;
        $product->carton = $request->carton;

        $product->save();

        $product->items()->attach($request->items);
        Toastr::success('Product Successfully Saved :)','Success');
        return redirect(route('admin.product.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show( Product $product)
    {
        return view('admin.product.show',compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {

        $items = Item::all();
        return view('admin.product.edit',compact('product','items'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Product $product)
    {
        $image = $request->file('image');

        if(isset($image))
        {
//            make unipue name for image
            $currentDate = Carbon::now()->toDateString();
            $imageName  = '-'.$currentDate.'-'.uniqid().'.'.$image->getClientOriginalExtension();
            if(!Storage::disk('public')->exists('product'))
            {
                Storage::disk('public')->makeDirectory('product');
            }
//            delete old product image
            if(Storage::disk('public')->exists('product/'.$product->image))
            {
                Storage::disk('public')->delete('product/'.$product->image);
            }
            $productImage = Image::make($image)->resize(1600,1066)->stream();
            Storage::disk('public')->put('product/'.$imageName,$productImage);
        } else {
            $imageName = $product->image;
        }

        $product->name= $request->name;
        $product->image = $imageName;
        $product->description = $request->description;
        $product->product_unit = $request->product_unit;
        $product->carton = $request->carton;
        $product->save();
        $product->items()->sync($request->items);
        Toastr::success('Product Successfully Updated :)','Success');
        return redirect()->route('admin.product.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( Product $product)
    {
        if (Storage::disk('public')->exists('product/'.$product->image))
        {
            Storage::disk('public')->delete('product/'.$product->image);
        }
//        $product->categories()->detach();
//        $product->tags()->detach();
        $product->delete();
        Toastr::success('Product Successfully Deleted :)','Success');
        return redirect()->back();
    }
}
