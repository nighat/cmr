<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;
use App\Supplier;
use App\Groupsupplier;
class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $suppliers= Supplier::latest()->get();
        return view('admin.supplier.index',compact('suppliers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $groupsuppliers = Groupsupplier::all();
        return view('admin.supplier.create',compact('groupsuppliers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $supplier = new Supplier;
        $supplier->name = $request->name;
        $supplier->p_name = $request->p_name;
        $supplier->phone = $request->phone;
        $supplier->email = $request->email;
        $supplier->address= $request->address;
        $supplier->city= $request->city;

        $supplier->save();

        $supplier->groupsuppliers()->attach($request->groupsuppliers);
        Toastr::success('Supplier Successfully Saved :)','Success');
        return redirect(route('admin.supplier.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Supplier $supplier)
    {
        return view('admin.supplier.show',compact('supplier'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $groupsuppliers = Groupsupplier::all();
        $supplier= Supplier::where('id',$id)->first();
        return view('admin.supplier.edit',compact('supplier','groupsuppliers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $supplier = Supplier::find($id);
        $supplier->name = $request->name;
        $supplier->p_name = $request->p_name;
        $supplier->phone = $request->phone;
        $supplier->email = $request->email;
        $supplier->address= $request->address;
        $supplier->city= $request->city;

        $supplier->save();
        $supplier->groupsuppliers()->sync($request->groupsuppliers);
        Toastr::success('Supplier Successfully Updated :)','Success');
        return redirect(route('admin.supplier.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Supplier::where('id',$id)->delete();
        return redirect()->back();
    }
}
