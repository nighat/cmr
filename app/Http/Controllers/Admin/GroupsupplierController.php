<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Groupsupplier;
use Brian2694\Toastr\Facades\Toastr;
class GroupsupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groupsuppliers= Groupsupplier::latest()->get();
        return view('admin.groupsupplier.index',compact('groupsuppliers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.groupsupplier.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',



        ]);

        $groupsupplier = new Groupsupplier;
        $groupsupplier->name = $request->name;
        $groupsupplier->save();
        Toastr::success('Supplier Group Successfully Create :)','Success');
        return redirect(route('admin.groupsupplier.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $groupsupplier= Groupsupplier::where('id',$id)->first();
        return view('admin.groupsupplier.edit',compact('groupsupplier'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name'=>'required',

        ]);

        $groupsupplier = Groupsupplier::find($id);
        $groupsupplier->name = $request->name;
        $groupsupplier->save();
        Toastr::success('Supplier Group Successfully Updated :)','Success');
        return redirect(route('admin.groupsupplier.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Groupsupplier::where('id',$id)->delete();
        return redirect()->back();
    }
}