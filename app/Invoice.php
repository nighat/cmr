<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $casts = [
        'attendees' => 'array',
    ];


    public function clients()
    {
        return $this->hasMany('App\Client');
    }
}
