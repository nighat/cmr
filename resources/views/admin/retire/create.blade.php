

@extends('layouts.backend.app')

@section('title','Return Product')

@push('css')
        <!-- Bootstrap Select Css -->
<link href="{{ asset('backend/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
@endpush

@section('content')
    <div class="container-fluid">
        <!-- Vertical Layout | With Floating Label -->
        <form action="{{ route('admin.retire.store') }}" method="POST" >
            {{ csrf_field() }}
            <div class="row clearfix">
                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Purchases Return Product
                            </h2>
                        </div>
                        <div class="body">
                            <div class="form-group form-float">
                                <div class="form-line">
                                <input type="date" id="date" class="form-control" name="date">
                                <label class="form-label">Date</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                <input type="number" id="return_quantity" class="form-control" name="return_quantity">
                                <label class="form-label">Return Quantity </label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                  <div class="form-line">
                                  <input type="number" id="buy_price" class="form-control" name="buy_price">
                                  <label class="form-label"> Buy Price</label>
                                 </div>
                            </div>

                            <div class="form-group form-float">
                                  <div class="form-line">
                                  <input type="text" id="note" class="form-control" name="note">
                                  <label class="form-label"> Note</label>
                                  </div>
                            </div>


                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header bg-blue">
                            <h2>
                              Supplier And Product
                            </h2>
                        </div>
                        <div class="body">
                            <div class="form-group form-float">
                                <div class="form-line {{ $errors->has('suppliers') ? 'focused error' : '' }}">
                                    <label for="supplier">Select Supplier</label>
                                    <select name="suppliers[]" id="supplier" class="form-control show-tick" data-live-search="true" multiple>
                                        @foreach($suppliers as $supplier)
                                            <option value="{{ $supplier->id }}">{{ $supplier->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="body">
                            <div class="form-group form-float">
                                <div class="form-line {{ $errors->has('products') ? 'focused error' : '' }}">
                                    <label for="product">Select Product</label>
                                    <select name="products[]" id="product" class="form-control show-tick" data-live-search="true" multiple>
                                        @foreach($products as $product)
                                            <option value="{{ $product->id }}">{{ $product->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <a  class="btn btn-danger m-t-15 waves-effect" href="{{ route('admin.purchase.index') }}">BACK</a>
                            <button type="submit" class="btn btn-primary m-t-15 waves-effect">SUBMIT</button>

                        </div>
                    </div>
                </div>
            </div>

        </form>
    </div>
    @endsection

    @push('js')
            <!-- Select Plugin Js -->
    <script src="{{ asset('backend/plugins/bootstrap-select/js/bootstrap-select.js') }}"></script>
    <!-- TinyMCE -->


    @endpush