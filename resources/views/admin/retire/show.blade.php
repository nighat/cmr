@extends('layouts.backend.app')

@section('title','Return Purchase')

@push('css')

@endpush

@section('content')
    <div class="container-fluid">
        <!-- Vertical Layout | With Floating Label -->
        <a href="{{ route('admin.retire.index') }}" class="btn btn-danger waves-effect">BACK</a>

        <br>
        <br>
        <div class="row clearfix">
            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header bg-cyan">
                        <h2>
                            Return Purcha

                        </h2>
                    </div>


                    <div class="body">
                        <b>Supplier Name:-</b>
                        @foreach($retire->suppliers as $supplier)

                            {{ $supplier->name }} <br/>
                        @endforeach
                        <b>Product Name:-</b>

                            @foreach($retire->products as $product)

                                {{ $product->name }} <br/>
                            @endforeach
                        <b>Note:-</b>
                        {{ $retire->note}}</br>

                       <b>Date:-</b>
                        {{ Carbon\Carbon::parse($retire->date)->format('d-m-Y ') }}<br/>

                        <b>Return Quantity:-</b>
                        {{ $retire->return_quantity }}<br/>
                        <b>Buy Price:-</b>
                         {{ $retire->buy_price}}.00<br/>


                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header bg-cyan">
                        <h2>
                            Total Buy Price
                        </h2>
                    </div>
                    <div class="body">
                        {{$retire->buy_price *$retire->return_quantity}}.00
                    </div>
                </div>
            </div>


        </div>
    </div>
    @endsection

    @push('js')
            <!-- Select Plugin Js -->
    <script src="{{ asset('backend/plugins/bootstrap-select/js/bootstrap-select.js') }}"></script>
    <!-- TinyMCE -->
    <script src="{{ asset('backend/plugins/tinymce/tinymce.js') }}"></script>
    <script src="https://unpkg.com/sweetalert2@7.19.1/dist/sweetalert2.all.js"></script>


    @endpush