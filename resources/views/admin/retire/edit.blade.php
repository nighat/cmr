
@extends('layouts.backend.app')

@section('title','Purchase Return')

@push('css')
        <!-- Bootstrap Select Css -->
<link href="{{ asset('backend/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
@endpush

@section('content')
    <div class="container-fluid">
        <!-- Vertical Layout | With Floating Label -->
        <form action="{{ route('admin.retire.update',$retire->id) }}" method="POST" >
            {{ csrf_field() }}
            {{ method_field('PATCH') }}
            <div class="row clearfix">
                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                EDIT RETURN PURCHASE
                            </h2>
                        </div>
                        <div class="body">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="date" id="date" class="form-control" name="date" value="<?php echo date("Y-m-d");?>">
                                    <label class="form-label">Date</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="number" id="return_quantity" class="form-control" name="return_quantity" value="{{ $retire->return_quantity }}">
                                    <label class="form-label">Return Quantity</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="number" id="buy_price" class="form-control" name="buy_price" value="{{ $retire->buy_price }}">
                                    <label class="form-label">Buy Price</label>
                                </div>
                            </div>

                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" id="note" class="form-control" name="note" value="{{ $retire->note}}">
                                    <label class="form-label">Note</label>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header bg-blue">
                            <h2>
                                Supplier And Product
                            </h2>
                        </div>
                        <div class="body">
                            <div class="form-group form-float">
                                <div class="form-line {{ $errors->has('suppliers') ? 'focused error' : '' }}">
                                    <label for="supplier">Select Supplier</label>
                                    <select name="suppliers[]" id="supplier" class="form-control show-tick" data-live-search="true" multiple>
                                        @foreach($suppliers as $supplier)
                                            <option
                                                    @foreach($retire->suppliers as $retire_supplier)
                                                    {{ $retire_supplier->id == $supplier->id ? 'selected' : '' }}
                                                    @endforeach
                                                    value="{{ $supplier->id }}">{{ $supplier->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="body">
                            <div class="form-group form-float">
                                <div class="form-line {{ $errors->has('products') ? 'focused error' : '' }}">
                                    <label for="product">Select Product</label>
                                    <select name="products[]" id="product" class="form-control show-tick" data-live-search="true" multiple>
                                        @foreach($products as $product)
                                            <option
                                                    @foreach($retire->products as $product_retire)
                                                    {{ $product_retire->id == $product->id ? 'selected' : '' }}
                                                    @endforeach
                                                    value="{{ $product->id }}">{{ $product->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <a  class="btn btn-danger m-t-15 waves-effect" href="{{ route('admin.retire.index') }}">BACK</a>
                            <button type="submit" class="btn btn-primary m-t-15 waves-effect">SUBMIT</button>

                        </div>
                    </div>
                </div>
            </div>

        </form>
    </div>
    @endsection

    @push('js')
            <!-- Select Plugin Js -->
    <script src="{{ asset('backend/plugins/bootstrap-select/js/bootstrap-select.js') }}"></script>
    <!-- TinyMCE -->
    <script src="{{ asset('backend/plugins/tinymce/tinymce.js') }}"></script>
    <script>
        $(function () {
            //TinyMCE
            tinymce.init({
                selector: "textarea#tinymce",
                theme: "modern",
                height: 300,
                plugins: [
                    'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                    'searchreplace wordcount visualblocks visualchars code fullscreen',
                    'insertdatetime media nonbreaking save table contextmenu directionality',
                    'emoticons template paste textcolor colorpicker textpattern imagetools'
                ],
                toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
                toolbar2: 'print preview media | forecolor backcolor emoticons',
                image_advtab: true
            });
            tinymce.suffix = ".min";
            tinyMCE.baseURL = '{{ asset('backend/plugins/tinymce') }}';
        });
    </script>

    @endpush


















