
@extends('layouts.backend.app')

@section('title','Post')

@push('css')
        <!-- Bootstrap Select Css -->
<link href="{{ asset('backend/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
@endpush

@section('content')
    <div class="container-fluid">
        <!-- Vertical Layout | With Floating Label -->
        <form action="{{ route('admin.client.update',$client->id) }}" method="POST">
            {{ csrf_field() }}
            {{ method_field('PATCH') }}
            <div class="row clearfix">
                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                EDIT POST
                            </h2>
                        </div>
                        <div class="body">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" id="name" class="form-control" name="name" value="{{ $client->name }}">
                                    <label class="form-label">Name</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" id="p_name" class="form-control" name="p_name" value="{{ $client->p_name }}">
                                    <label class="form-label"> Proprietor Name</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="number" id="phone" class="form-control" name="phone" value="{{ $client->phone }}">
                                    <label class="form-label"> Phone Number</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="email" id="email" class="form-control" name="email" value="{{ $client->email}}">
                                    <label class="form-label"> E-mail</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" id="address" class="form-control" name="address" value="{{ $client->address}}">
                                    <label class="form-label"> Address</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" id="city" class="form-control" name="city" value="{{ $client->city }}">
                                    <label class="form-label"> City</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Group
                            </h2>
                        </div>
                        <div class="body">


                            <div class="form-group form-float">

                                <div class="form-line {{ $errors->has('groupclients') ? 'focused error' : '' }}">
                                    <label for="groupclient">Select Category</label>
                                    <select name="groupclients[]" id="groupclient" class="form-control show-tick" data-live-search="true" multiple>
                                        @foreach($groupclients as $groupclient)
                                            <option
                                                    @foreach($client->groupclients as $cgroupclient)
                                                    {{ $cgroupclient->id == $groupclient->id ? 'selected' : '' }}
                                                    @endforeach
                                                    value="{{ $groupclient->id }}">{{ $groupclient->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>



                            <a  class="btn btn-danger m-t-15 waves-effect" href="{{ route('admin.client.index') }}">BACK</a>
                            <button type="submit" class="btn btn-primary m-t-15 waves-effect">SUBMIT</button>

                        </div>
                    </div>
                </div>
            </div>

        </form>
    </div>
    @endsection

    @push('js')
            <!-- Select Plugin Js -->
    <script src="{{ asset('backend/plugins/bootstrap-select/js/bootstrap-select.js') }}"></script>
    <!-- TinyMCE -->
    <script src="{{ asset('backend/plugins/tinymce/tinymce.js') }}"></script>
    <script>
        $(function () {
            //TinyMCE
            tinymce.init({
                selector: "textarea#tinymce",
                theme: "modern",
                height: 300,
                plugins: [
                    'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                    'searchreplace wordcount visualblocks visualchars code fullscreen',
                    'insertdatetime media nonbreaking save table contextmenu directionality',
                    'emoticons template paste textcolor colorpicker textpattern imagetools'
                ],
                toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
                toolbar2: 'print preview media | forecolor backcolor emoticons',
                image_advtab: true
            });
            tinymce.suffix = ".min";
            tinyMCE.baseURL = '{{ asset('backend/plugins/tinymce') }}';
        });
    </script>

    @endpush


















