@extends('layouts.backend.app')

@section('title','purchase')

@push('css')

@endpush

@section('content')
    <div class="container-fluid">
        <!-- Vertical Layout | With Floating Label -->
        <a href="{{ route('admin.purchase.index') }}" class="btn btn-danger waves-effect">BACK</a>

        <br>
        <br>
        <div class="row clearfix">
            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header bg-cyan">
                        <h2>
                            Purchase Details

                        </h2>
                    </div>


                    <div class="body">
                        <b>Supplier Name:-</b>
                        @foreach($purchase->suppliers as $supplier)

                            {{ $supplier->name }} <br/>
                        @endforeach
                        <b>Product Name:-</b>

                            @foreach($purchase->products as $product)

                                {{ $product->name }} <br/>
                            @endforeach
                        <b>Note:-</b>
                        {{ $purchase->note}}</br>

                       <b>Date:-</b>
                        {{ Carbon\Carbon::parse($purchase->date)->format('d-m-Y ') }}<br/>

                        <b>Purchase Quantity:-</b>
                        {{ $purchase->purchase_quantity }}<br/>
                        <b>Buy Price:-</b>
                         {{ $purchase->buy_price}}.00<br/>
                        <b>Sell Price:-</b>
                         {{ $purchase->sell_price}}.00<br/>

                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header bg-cyan">
                        <h2>
                            Total Buy Price
                        </h2>
                    </div>
                    <div class="body">
                        {{$purchase->buy_price *$purchase->purchase_quantity}}.00
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header bg-orange">
                        <h2>
                            Total Sell Price

                        </h2>
                    </div>
                    <div class="body">
                        {{$purchase->sell_price *$purchase->purchase_quantity}}.00
                    </div>
                </div>
            </div>

        </div>
    </div>
    @endsection

    @push('js')
            <!-- Select Plugin Js -->
    <script src="{{ asset('backend/plugins/bootstrap-select/js/bootstrap-select.js') }}"></script>
    <!-- TinyMCE -->
    <script src="{{ asset('backend/plugins/tinymce/tinymce.js') }}"></script>
    <script src="https://unpkg.com/sweetalert2@7.19.1/dist/sweetalert2.all.js"></script>


    @endpush