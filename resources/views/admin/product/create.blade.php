

@extends('layouts.backend.app')

@section('title','Product')

@push('css')
        <!-- Bootstrap Select Css -->
<link href="{{ asset('backend/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
@endpush

@section('content')
    <div class="container-fluid">
        <!-- Vertical Layout | With Floating Label -->
        <form action="{{ route('admin.product.store') }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="row clearfix">
                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                 PRODUCT ENTRY
                            </h2>
                        </div>
                        <div class="body">
                            <div class="form-group form-float">
                                <div class="form-line">
                                <input type="text" id="name" class="form-control" name="name">
                                <label class="form-label">Name</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                <input type="text" id="description" class="form-control" name="description">
                                <label class="form-label">Product Description </label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                  <div class="form-line">
                                  <input type="number" id="product_unit" class="form-control" name="product_unit">
                                  <label class="form-label"> Product Unit</label>
                                 </div>
                            </div>
                            <div class="form-group form-float">
                                  <div class="form-line">
                                  <input type="number" id="carton" class="form-control" name="carton">
                                  <label class="form-label"> Carton</label>
                                  </div>
                            </div>

                            <div class="form-group">
                                <label for="image">Product Image</label>
                                <input type="file" name="image">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                               Item Name
                            </h2>
                        </div>
                        <div class="body">
                            <div class="form-group form-float">
                                <div class="form-line {{ $errors->has('items') ? 'focused error' : '' }}">
                                    <label for="items">Select Group</label>
                                    <select name="items[]" id="items" class="form-control show-tick" data-live-search="true" multiple>
                                        @foreach($items as $item)
                                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <a  class="btn btn-danger m-t-15 waves-effect" href="{{ route('admin.product.index') }}">BACK</a>
                            <button type="submit" class="btn btn-primary m-t-15 waves-effect">SUBMIT</button>

                        </div>
                    </div>
                </div>
            </div>

        </form>
    </div>
    @endsection

    @push('js')
            <!-- Select Plugin Js -->
    <script src="{{ asset('backend/plugins/bootstrap-select/js/bootstrap-select.js') }}"></script>
    <!-- TinyMCE -->


    @endpush