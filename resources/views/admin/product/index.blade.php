
@extends('layouts.backend.app')
@section('title','Product')

@push('css')
        <!-- JQuery DataTable Css -->
<link href="{{ asset('backend/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
@endpush
@section('content')
    <div class="container-fluid">
        <div class="block-header">
            <a  class="btn bg-primary waves-effect " href="{{ route('admin.product.create') }}">
                <i class="material-icons">add</i>
                <span>Add New product </span>
            </a>
        </div>
        <!-- #END# Basic Examples -->
        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                             Product List
                            <span class="badge bg-blue">{{ $products->count() }}</span>
                        </h2>

                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <thead>
                                <tr>

                                    <th>Product</th>
                                    <th>Image </th>
                                    <th>Quantity </th>
                                    <th>T. Buy Price</th>
                                    <th>T. Sell Price</th>
                                    <th>Group Name</th>
                                    <th>Action</th>

                                </tr>
                                </thead>
                                <tfoot>
                                <tr>

                                    <th>Product</th>
                                    <th>Image </th>
                                    <th>Quantity </th>
                                    <th>T. Buy Price</th>
                                    <th>T. Sell Price</th>
                                    <th>Group Name</th>
                                    <th>Action</th>
                                </tr>
                                </tfoot>
                                <tbody>



                                @foreach($products as $product)

                                    <tr>

                                        <td>
                                            {{ $product->name}}<br>
                                            {{ $product->description}}<br>
                                            <b>Buy Price:</b>
                                            @foreach($product->purchases as $purchase)

                                                {{ $purchase->buy_price  }} <br/>
                                            @endforeach
                                           <b>Sell Price:</b>
                                            @foreach($product->purchases as $purchase)

                                                {{ $purchase->sell_price  }} <br/>
                                            @endforeach

                                        </td>
                                        <td > <img style="width:50px;height:50px; class="img-responsive thumbnail" src="{{ Storage::disk('public')->url('product/'.$product->image) }}" alt=""> </td>
                                        <td>
                                            @foreach($product->purchases as $purchase)

                                                {{ $purchase->purchase_quantity  }} <br/>
                                            @endforeach
                                        </td>
                                        <td>
                                            @foreach($product->purchases as $purchase)

                                                {{ $purchase->buy_price *$purchase->purchase_quantity }} <br/>
                                            @endforeach
                                        </td>

                                        <td>
                                            @foreach($product->purchases as $purchase)

                                                {{ $purchase->sell_price *$purchase->purchase_quantity }} <br/>
                                            @endforeach
                                        </td>
                                        <td>
                                            @foreach($product->items as $item)

                                                {{ $item->name }} <br/>
                                            @endforeach
                                        </td>



                                        <td class="text-center">
                                            <a href="{{ route('admin.product.show',$product->id) }}" class="btn btn-info waves-effect">
                                                <i class="material-icons">visibility</i>
                                            </a>
                                            <a href="{{route('admin.product.edit',$product->id)}}" class="btn btn-primary waves-effect" >
                                                <i class="material-icons">edit</i>

                                            </a>
                                            <button class="btn btn-danger waves-effect" type="button" onclick="deleteproduct({{ $product->id }})">
                                                <i class="material-icons">delete</i>
                                            </button>
                                            <form id="delete-form-{{ $product->id }}" action="{{ route('admin.product.destroy',$product->id) }}" method="POST" style="display: none;">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                            </form>
                                        </td>
                                    </tr>

                                @endforeach

                                </tbody>



                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- #END# Exportable Table -->
    </div>
    @endsection

    @push('js')
            <!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('backend/plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('backend/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('backend/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('backend/plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>
    <script src="{{ asset('backend/js/pages/tables/jquery-datatable.js') }}"></script>

    <script src="https://unpkg.com/sweetalert2@7.19.1/dist/sweetalert2.all.js"></script>
    <script type="text/javascript">
        function deleteproduct(id) {
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false,
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                event.preventDefault();
                document.getElementById('delete-form-'+id).submit();
            }else if (
                    // Read more about handling dismissals
            result.dismiss === swal.DismissReason.cancel
            ) {
                swal(
                        'Cancelled',
                        'Your data is safe :)',
                        'error'
                )
            }
        })
        }
    </script>
    @endpush














