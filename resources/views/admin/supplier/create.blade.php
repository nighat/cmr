

@extends('layouts.backend.app')

@section('title','Post')

@push('css')
        <!-- Bootstrap Select Css -->
<link href="{{ asset('backend/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
@endpush

@section('content')
    <div class="container-fluid">
        <!-- Vertical Layout | With Floating Label -->
        <form action="{{ route('admin.supplier.store') }}" method="POST">
            {{ csrf_field() }}
            <div class="row clearfix">
                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                ADD NEW SUPPLIER
                            </h2>
                        </div>
                        <div class="body">
                            <div class="form-group form-float">
                                <div class="form-line">
                                <input type="text" id="name" class="form-control" name="name">
                                <label class="form-label">Name</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                <input type="text" id="p_name" class="form-control" name="p_name">
                                <label class="form-label"> Proprietor Name</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                  <div class="form-line">
                                  <input type="number" id="phone" class="form-control" name="phone">
                                  <label class="form-label"> Phone Number</label>
                                 </div>
                            </div>
                            <div class="form-group form-float">
                                  <div class="form-line">
                                  <input type="email" id="email" class="form-control" name="email">
                                  <label class="form-label"> E-mail</label>
                                  </div>
                            </div>
                            <div class="form-group form-float">
                                  <div class="form-line">
                                   <input type="text" id="address" class="form-control" name="address">
                                  <label class="form-label"> Address</label>
                                 </div>
                            </div>
                            <div class="form-group form-float">
                                  <div class="form-line">
                                  <input type="text" id="city" class="form-control" name="city">
                                  <label class="form-label"> City</label>
                                 </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                               Group
                            </h2>
                        </div>
                        <div class="body">
                            <div class="form-group form-float">
                                <div class="form-line {{ $errors->has('groupsuppliers') ? 'focused error' : '' }}">
                                    <label for="groupsupplier">Select Group</label>
                                    <select name="groupsuppliers[]" id="groupsupplier" class="form-control show-tick" data-live-search="true" multiple>
                                        @foreach($groupsuppliers as $groupsupplier)
                                            <option value="{{ $groupsupplier->id }}">{{ $groupsupplier->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <a  class="btn btn-danger m-t-15 waves-effect" href="{{ route('admin.supplier.index') }}">BACK</a>
                            <button type="submit" class="btn btn-primary m-t-15 waves-effect">SUBMIT</button>

                        </div>
                    </div>
                </div>
            </div>

        </form>
    </div>
    @endsection

    @push('js')
            <!-- Select Plugin Js -->
    <script src="{{ asset('backend/plugins/bootstrap-select/js/bootstrap-select.js') }}"></script>
    <!-- TinyMCE -->


    @endpush