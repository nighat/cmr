@extends('layouts.backend.app')

@section('title','Invoice')

@push('css')
        <!-- Bootstrap Select Css -->
<link href="{{ asset('backend/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />

@endpush

@section('content')
    <div class="container-fluid">

        <form action="{{ route('admin.invoice.update',$invoice->id) }}" method="POST">
            {{ csrf_field() }}
            {{ method_field('PATCH') }}
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header bg-cyan">
                            <h2>
                                ADD NEW INVOICE
                            </h2>
                        </div>

                        <div class="body">
                            <div class="form-group form-float">
                                <address class="pull-right" >
                                    <p>Jonathan Neal</p>
                                    <p>101 E. Chapman Ave<br>Orange, CA 92866</p>
                                    <p>(800) 555-1234</p>
                                </address>
                            </div>
                        </div>
                        <div class="body">
                            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-4">

                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="date" id="date" class="form-control" name="date" value="{{ $invoice->date }}">
                                        <label class="form-label">Date</label>
                                    </div>
                                </div>

                                {{--<div class="form-group">--}}
                                {{--<label for="unique_id" class="col-md-4 control-label">unique_id</label>--}}

                                {{--<div class="col-md-6 ">--}}
                                {{--<input id="client_id" type="hidden" class="form-control " name="client_id">--}}
                                {{--</div>--}}
                                {{--</div>--}}
                            </div>
                        </div>
                        <div class="body">
                            <table  class="table table-hover small-text" id="tb">
                                <tr class="tr-header">
                                    <th>Client Name</th>
                                    <th>Product Name</th>
                                    <th>Price</th>
                                    <th>Quantity</th>
                                    <th><a href="javascript:void(0);" style="font-size:18px;" id="addMore" title="Add More Person"><span class="glyphicon glyphicon-plus"></span></a></th>
                                <tr>
                                    <td>
                                        <select class="form-control select2 select2-hidden-accessible" data-placeholder="Select a State" style="width: 100%;" tabindex="-1" aria-hidden="true" name="post_id">
                                            @foreach($clients as $client)
                                                <option value="{{ $client->id }}">{{ $client->name}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    {{--<td><select name="client" class="form-control">--}}
                                    {{--<option value={{$client->client_id}} selected>Select Client</option>--}}
                                    {{--@foreach($clients as $client)--}}
                                    {{--<option value="{{ $client->id }}">{{ $client->name}}</option>--}}
                                    {{--@endforeach--}}
                                    {{--</select>--}}
                                    {{--</td>--}}
                                    <td><input type="text" name="name[]" class="form-control" value="{{ $invoice->name }}"></td>
                                    <td><input type="number" name="price[]" class="form-control" value="{{ $invoice->price }}"></td>
                                    <td><input type="number" name="qty[]" class="form-control" value="{{ $invoice->qty }}"></td>
                                    {{--<td><input type="text" name="emailid[]" class="form-control"></td>--}}
                                    <td><a href='javascript:void(0);'  class='remove'><span class='glyphicon glyphicon-remove'></span></a></td>
                                </tr>

                            </table>

                            <a  class="btn btn-danger m-t-15 waves-effect" href="{{ route('admin.invoice.index') }}">BACK</a>
                            <button type="submit" class="btn btn-primary m-t-15 waves-effect">SUBMIT</button>
                        </div>

                    </div>
                </div>

            </div>

        </form>
    </div>
    @endsection

    @push('js')
            <!-- Select Plugin Js -->
    <script src="{{ asset('backend/plugins/bootstrap-select/js/bootstrap-select.js') }}"></script>
    <!-- TinyMCE -->
    <script>
        $(function(){
            $('#addMore').on('click', function() {
                var data = $("#tb tr:eq(1)").clone(true).appendTo("#tb");
                data.find("input").val('');
            });
            $(document).on('click', '.remove', function() {
                var trIndex = $(this).closest("tr").index();
                if(trIndex>1) {
                    $(this).closest("tr").remove();
                } else {
                    alert("Sorry!! Can't remove first row!");
                }
            });
        });
    </script>

    @endpush