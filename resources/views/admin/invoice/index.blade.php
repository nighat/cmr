@extends('layouts.master')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="clearfix">
                <span class="panel-title">Invoices</span>
                <a href="{{route('admin.invoice.create')}}" class="btn btn-success pull-right">Create</a>
            </div>
        </div>
        <div class="panel-body">
            @if($invoices->count())
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Invoice No.</th>
                        <th>Date</th>
                        <th>Client Name</th>
                        <th>Product Name</th>
                        <th>Price</th>
                        <th>QTY</th>
                        <th>Total</th>

                    </tr>
                </thead>
                <tbody>
                    @foreach($invoices as $invoice)
                        <tr>
                            <td>{{$invoice->unique_id}}</td>
                            <td>{{$invoice->date}}</td>
                            {{--<td>@foreach($invoice->clients as $client)--}}

                                    {{--{{ $client->name }} |--}}
                                {{--@endforeach</td>--}}
                            <td>{{$invoice->name}}</td>
                            <td>{{$invoice->price}}.00</td>
                            <td>{{$invoice->qty}}</td>
                            <td>{{$invoice->price*$invoice->qty}}.00</td>
                            <td class="text-right">
                                <a href="{{route('admin.invoice.show', $invoice)}}" class="btn btn-default btn-sm">View</a>
                                <a href="{{route('admin.invoice.edit', $invoice)}}" class="btn btn-primary btn-sm">Edit</a>
                                <form class="form-inline" method="post"
                                    action="{{route('admin.invoice.destroy', $invoice)}}"
                                    onsubmit="return confirm('Are you sure?')"
                                >
                                    <input type="hidden" name="_method" value="delete">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{--{!! $invoices->render() !!}--}}
            @else
                <div class="invoice-empty">
                    <p class="invoice-empty-title">
                        No Invoices were created.
                        <a href="{{route('admin.invoice.create')}}">Create Now!</a>
                    </p>
                </div>
            @endif
        </div>
    </div>
@endsection